window.addEventListener("DOMContentLoaded", async () => {
  const url = "http://localhost:8000/api/states/";

  const response = await fetch(url);
  // Get the select tag element by its id 'state'

  // For each state in the states property of the data

  // Create an 'option' element

  // Set the '.value' property of the option element to the
  // state's abbreviation

  // Set the '.innerHTML' property of the option element to
  // the state's name

  // Append the option element as a child of the select tag

  if (response.ok) {
    const data = await response.json();
    const selector = document.getElementById("state");
    for (let state of data.states) {
      const stateValue = document.createElement("option");
      stateValue.value = state.abbreviation;
      stateValue.innerHTML = state.name;
      selector.appendChild(stateValue);
    }
    const formTag = document.getElementById("create-location-form");
    formTag.addEventListener("submit",  async event => {
      event.preventDefault();
      // console.log('need to submit the form data');
      const formData = new FormData(formTag);
      const json = JSON.stringify(Object.fromEntries(formData));

    try{
    const locationUrl = "http://localhost:8000/api/locations/";
    const fetchConfig = {
      method: "post",
      body: json,
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
      formTag.reset();
      const newLocation = await response.json();
      console.log(newLocation);
    }
  } catch (err) {
    console.log(err);
  }
});
}
});

// You open the app.js file for reference. Your teammate starts to dictate the outline of the file. After each statement, you type the code to make that happen, usually with a console.log statement to make sure it worked. After you type code for each instruction, you refresh the page to make sure your JavaScript worked.

// "We need to add an event listener for when the DOM loads."
// "Let's declare a variable that will hold the URL for the API that we just created."
// "Let's fetch the URL. Don't forget the await keyword so that we get the response, not the Promise."
// "If the response is okay, then let's get the data using the .json method. Don't forget to await that, too."
