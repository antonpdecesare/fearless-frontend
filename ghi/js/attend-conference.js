window.addEventListener("DOMContentLoaded", async () => {
  const selectTag = document.getElementById("conference");

  const url = "http://localhost:8000/api/conferences/";
  const response = await fetch(url);
  if (response.ok) {
    const data = await response.json();

    for (let conference of data.conferences) {
      const option = document.createElement("option");
      option.value = conference.href;
      option.innerHTML = conference.name;
      selectTag.appendChild(option);
    }
    const loadingIcon = document.getElementById("loading-conference-spinner");
    loadingIcon.classList.add("d-none");

    selectTag.classList.remove("d-none");
  }

  const formTag = document.getElementById("create-attendee-form");
  formTag.addEventListener("submit", async (event) => {
    event.preventDefault();
    // console.log('need to submit the form data');
    const formData = new FormData(formTag);
    const json = JSON.stringify(Object.fromEntries(formData));

    try {
      const attendeeUrl = "http://localhost:8001/api/attendees/";
      const fetchConfig = {
        method: "post",
        body: json,
        headers: {
          "Content-Type": "application/json",
        },
      };
      const response = await fetch(attendeeUrl, fetchConfig);
      if (response.ok) {
        formTag.reset();
        const newAttendee = await response.json();
        console.log(newAttendee)
        const successAlert = document.getElementById("success-message");
        successAlert.classList.remove("d-none");

        formTag.classList.add("d-none");
      }
    } catch (err) {
      console.log(err);
    }
  });
});
  // 1. Get the attendee form element by its id
  // 2. Add an event handler for the submit event
  // 3. Prevent the default from happening
  // 4. Create a FormData object from the form
  // 5. Get a new object from the form data's entries
  // 6. Create options for the fetch
  // 7. Make the fetch using the await keyword to the URL http://localhost:8001/api/attendees/
  // Get the attendee form element by its id

  // Add an event handler for the submit event

  // Prevent the default from happening

  // Create a FormData object from the form

  // Get a new object from the form data's entries

  // Create options for the fetch

  // Make the fetch using the await keyword to the URL http://localhost:8001/api/attendees/
